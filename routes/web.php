<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::group(['middleware' => ['auth']], function () {    
// };

Route::get('/', function () {
    return view('login.login');
});

Route::get('/home', function(){
    return view('home.home');
});

Route::get('/contact',function(){
    return view('home.contact');
});

Route::get('/profil',function(){
    return view('home.profil');
});

Route::get('/topup/create', 'TopupController@create');
Route::post('/topup', 'TopupController@store');
Route::get('/topup', 'TopupController@index');

Route::get('/user/create', 'UserController@create');

Route::get('/transaksi/create', 'TransaksiController@create');
Route::post('/transaksi', 'TransaksiController@store');
Route::get('/history','HistoryController@create');
Route::get('/login',function(){
    return view('login.login');
});

Route::get('/erd', function(){
    return view('home.erd');
});

//Crud User
Route::get('/user', 'userController@index');
Route::get('/user/create', 'userController@create');
Route::post('/user', 'userController@store');
Route::get('/user/(user_id)', 'userController@show');