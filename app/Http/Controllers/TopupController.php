<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class TopupController extends Controller
{
    public function create(){
        return view('home.saldo');
    }

    public function store(Request $req){
        $req -> validate([
            'jumlah' => 'required',
            'metode' => 'required'
        ]);
        $query = DB::table('topup')->insert([
            "jumlah"=>$req["jumlah"],
            "metode"=>$req["metode"]
        ]);
        return redirect('/topup/create')->with('success','Post Berhasil Disimpan');
    }

    public function index(){
        $topup = DB::table('topup')->get();
        return view('home.history',compact('topup'));
    }
}
