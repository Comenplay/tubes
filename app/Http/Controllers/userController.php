<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class userController extends Controller
{
    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        $query = DB::table('user')->insert([
            "nama" => $request["nama"],
            "email" => $request["email"],
            "password" => $request["password"]
        ]);
        return redirect('/user');
    }
    public function index()
    {
        $user = DB::table('user')->get();
        return view('/user.index', compact('user'));
    }

    public function show($id)
    {
        $user = DB::table('user')->where('id', $id)->first();
        return view('/user.show', compact('user'));
    }
}
