<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class TransaksiController extends Controller
{
    public function create(){
        return view('home.topup');
    }

    public function store(Request $req){
        $req -> validate([
            'server' => 'required',
            'hp' => 'required',
            'diamond' => 'required',
            'id_topup' => 'required'
        ]);
        $query = DB::table('transaksi')->insert([
            "server"=>$req["server"],
            "hp"=>$req["hp"],
            "diamond"=>$req["diamond"],
            "id_topup"=>$req["id_topup"],
        ]);
        return redirect('/transaksi/create')->with('success','Post Berhasil Disimpan');

    }
}
