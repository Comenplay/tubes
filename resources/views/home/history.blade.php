@extends('adminlte.master')

@section('content')
    <div class="card">
        <div class="card-body table-responsive p-0 ml-3 mt-3 ">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Jumlah</th>
                        <th>Metode</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($topups as $key => $topup)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$topup-> $jumlah}}</td>
                            <td>{{$topup-> $metode}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection