@extends('adminlte.master')

@section('content')
    <div>
        <body class="is-boxed has-animations">
            <div class="body-wrap boxed-container">
                <header class="site-header">
                    <div class="container">
                        <div class="site-header-inner">
                            <div class="brand header-brand">
                                <h1 class="m-0">
                                    <a href="#">
                                        <img class="header-logo-image asset-light" src="{{asset('dist/images/logo-light.svg')}}" alt="Logo">
                                        <img class="header-logo-image asset-dark" src="{{asset('dist/images/logo-dark.svg')}}" alt="Logo">
                                    </a>
                                </h1>
                            </div>
                        </div>
                    </div>
                </header>

                <main>
                    <section class="hero">
                        <div class="container">
                            <div class="hero-inner">
                                <div class="hero-copy">
                                    <h1 class="hero-title mt-0">Top Up Diamond<br>Asiva Store</h1>
                                    <p class="hero-paragraph">Top Up Diamond Legal, Termurah, Tercepat se Instagram</p>
                                    <div class="hero-cta">
                                        <div class="lights-toggle">
                                            <input id="lights-toggle" type="checkbox" name="lights-toggle" class="switch" checked="checked">
                                            <label for="lights-toggle" class="text-xs"><span>Turn me <span class="label-text">dark</span></span></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="hero-media">
                                    <div class="header-illustration">
                                        <img class="header-illustration-image asset-light" src="{{asset('dist/images/header-illustration-light.svg')}}" alt="Header illustration">
                                        <img class="header-illustration-image asset-dark" src="{{asset('dist/images/header-illustration-dark.svg')}}" alt="Header illustration">
                                    </div>
                                    <div class="hero-media-illustration">
                                        <img class="hero-media-illustration-image asset-light" src="{{asset('dist/images/hero-media-illustration-light.svg')}}" alt="Hero media illustration">
                                        <img class="hero-media-illustration-image asset-dark" src="{{asset('dist/images/hero-media-illustration-dark.svg')}}" alt="Hero media illustration">
                                    </div>
                                </div>
                                <div class="features-image">
                                    <img class="features-box asset-light" src="{{asset('img/img-4.webp')}}" alt="Feature box">
                                    <img class="features-box asset-dark" src="{{asset('img/img-5.webp')}}" alt="Feature box">
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="features section">
                        <div class="container">
                            <div class="features-inner section-inner has-bottom-divider">
                                <div class="features-header text-center">
                                    <div class="container-sm">
                                        <h2 class="section-title mt-0">Kita Jualan Apa sih ?</h2>
                                        <p class="section-paragraph">Jualan Diamond Mobile Legend Ngabs, Bukan Jualan Gorengan</p>
                                        <div class="features-image">
                                            <img class="features-illustration asset-dark" src="{{asset('dist/images/features-illustration-dark.svg')}}" alt="Feature illustration">
                                            <img class="features-box asset-dark" src="{{asset('img/img-4.webp')}}" alt="Feature box">
                                            <img class="features-illustration asset-dark" src="{{asset('dist/images/features-illustration-top-dark.svg')}}" alt="Feature illustration top">
                                            <img class="features-illustration asset-light" src="{{asset('dist/images/features-illustration-light.svg')}}" alt="Feature illustration">
                                            <img class="features-box asset-light" src="{{asset('img/img-5.webp')}}" alt="Feature box">
                                            <img class="features-illustration asset-light" src="{{asset('dist/images/features-illustration-top-light.svg')}}" alt="Feature illustration top">
                                        </div>
                                    </div>
                                </div>
                                <div class="features-wrap">
                                    <div class="feature is-revealing">
                                        <div class="feature-inner">
                                            <div class="feature-icon">
                                                <img class="asset-light" src="{{asset('dist/images/feature-01-light.svg')}}" alt="Feature 01">
                                                <img class="asset-dark" src="{{asset('dist/images/feature-01-dark.svg')}}" alt="Feature 01">
                                            </div>
                                            <div class="feature-content">
                                                <h3 class="feature-title mt-0">Diamond Legal</h3>
                                                <p class="text-sm mb-0">Kalau ada yang lebih murah daripada kita, itu ga legal ngab, karna kita udah yang termurah</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="feature is-revealing">
                                        <div class="feature-inner">
                                            <div class="feature-icon">
                                                <img class="asset-light" src="{{asset('dist/images/feature-02-light.svg')}}" alt="Feature 02">
                                                <img class="asset-dark" src="{{asset('dist/images/feature-02-dark.svg')}}" alt="Feature 02">
                                            </div>
                                            <div class="feature-content">
                                                <h3 class="feature-title mt-0">Termurah</h3>
                                                <p class="text-sm mb-0">Kalau Kurang Murah, Kita Murahkan Lagi</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="feature is-revealing">
                                        <div class="feature-inner">
                                            <div class="feature-icon">
                                                <img class="asset-light" src="{{asset('dist/images/feature-03-light.svg')}}" alt="Feature 03">
                                                <img class="asset-dark" src="{{asset('dist/images/feature-03-dark.svg')}}" alt="Feature 03">
                                            </div>
                                            <div class="feature-content">
                                                <h3 class="feature-title mt-0">Tercepat</h3>
                                                <p class="text-sm mb-0">Orang 5-15 menit, di kita < 5 menit selesai</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="cta section">
                        <div class="container-sm">
                            <div class="cta-inner section-inner">
                                <div class="cta-header text-center">
                                    <h2 class="section-title mt-0">Top Up Sekarang</h2>
                                    <p class="section-paragraph">Top Up ga ? Top Up Ga ? Top Up lah masa Nggak!!</p>
                                    <div class="cta-cta">
                                        <a class="button button-primary" href="/top-up">Top Up</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>
            </div>
            <script src="{{asset('dist/js/main.min.js')}}"></script>
        </body>
        </html>

    </div>
@endsection