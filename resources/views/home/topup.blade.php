@extends('adminlte.master')

@section('content')
    <div class="card card-solid">
        <div class="card-body pb-0">
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Top Up Diamond Fast</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/transaksi" method="POST">
                  @csrf
                  <div class="card-body">
                    <div class="form-row">
                        <div class="col">
                            <label for="exampleInputEmail1">Server</label>
                            <input type="text" class="form-control" id="server" name="server">
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nomor HP</label>
                      <input type="text" class="form-control" id="hp" name="hp" placeholder="08....">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Masukan Jumalah Diamond</label>
                      <input type="number" class="form-control" id="diamond" name="diamond">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Top Up ID</label>
                      <input type="number" class="form-control" id="id_topup" name="id_topup">
                    </div>
                  </div>
                  <!-- /.card-body -->
  
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
        </div>
    </div>
@endsection