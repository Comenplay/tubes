@extends('adminlte.master')

@section('content')
<div class="card card-solid">
    <div class="card-body pb-0">
        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Top Up Saldo</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/topup" method="POST">
              @csrf
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Masukkan Jumlah Koin</label>
                  <input type="number" class="form-control" id="jumlah" name="jumlah" value="{{old('jumlah', '')}}">
                  @error('jumlah')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Masukan Metode Pembayaran</label>
                  <input type="text" class="form-control" id="metode" name="metode" value="{{old('metode', '')}}">
                  @error('metode')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
    </div>
</div>
@endsection