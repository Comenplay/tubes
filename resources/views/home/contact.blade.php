@extends('adminlte.master')

@section('content')
        <section class="content">

        <!-- Default box -->
        <div class="card card-solid">
            <div class="card-body pb-0">
            <div class="row d-flex align-items-stretch">
                <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                    <div class="card bg-light">
                        <div class="card-header text-muted border-bottom-0">
                        Wibu Akud
                        </div>
                        <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-7">
                            <h2 class="lead"><b>M. Rezza Alfarizy</b></h2>
                            <p class="text-muted text-sm"><b>About: </b> Full Stack Developer </p>
                            <ul class="ml-4 mb-0 fa-ul text-muted">
                                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Address: Guguak Sarai</li>
                                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone : 0812 3456 7890</li>
                            </ul>
                            </div>
                            <div class="col-5 text-center">
                            <img src="{{asset('img/img-2.jpg')}}" alt="" class="img-circle img-fluid">
                            </div>
                        </div>
                        </div>
                        <div class="card-footer">
                        <div class="text-right">
                            <a href="#" class="btn btn-sm bg-teal">
                            <i class="fas fa-comments"></i>
                            </a>
                            <a href="#" class="btn btn-sm btn-primary">
                            <i class="fas fa-user"></i> View Profile
                            </a>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                    <div class="card bg-light">
                        <div class="card-header text-muted border-bottom-0">
                        Player Mobile Legend
                        </div>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="lead"><b>Irfanul Arifa</b></h2>
                                    <p class="text-muted text-sm"><b>About: </b> Pro-gamer - Programmer </p>
                                    <ul class="ml-4 mb-0 fa-ul text-muted">
                                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Address: Pariaman</li>
                                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone : +62 822 8092 9092</li>
                                    </ul>
                                    </div>
                                    <div class="col-5 text-center">
                                    <img src="{{asset('img/img-3.png')}}" alt="" class="img-circle img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <a href="https://wa.me/6282285779829" class="btn btn-sm bg-teal">
                                <i class="fas fa-comments"></i>
                                </a>
                                <a href="#" class="btn btn-sm btn-primary">
                                <i class="fas fa-user"></i> View Profile
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
            <!-- /.card-footer -->
        </div>
        <!-- /.card -->

        </section>
@endsection