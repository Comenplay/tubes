<div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="/profil" class="nav-link">
            <i class="nav-icon fas fa-user-circle"></i>
            <p>
              Cek Profil
              <i class="right fas fa-angle"></i>
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/transaksi/create" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Top Up Diamond Fast
              <i class="right fas fa-angle"></i>
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/topup" class="nav-link">
            <i class="nav-icon fas fa-chart-pie"></i>
            <p>
              Cek History
              <i class="fas fa-angle"></i>
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/topup/create" class="nav-link">
            <i class="nav-icon fas fa-edit"></i>
            <p>
              Top Up Saldo
              <i class="fas fa-angle"></i>
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>