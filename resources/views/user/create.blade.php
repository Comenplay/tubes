@extends('adminlte.master')
@section('title')
    
@endsection
@section('content')
<div>
    <h2>Tambah User</h2>
        <form action="/user" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Masukan Nama</label>
                <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Email</label>
                <input type="text" class="form-control" name="email" id="body" placeholder="Masukkan email">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Masukan Password</label>
                <input type="text" class="form-control" name="password" id="title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection