@extends('adminlte.master')
@section('title')
    List user
@endsection

@section('content')
<a href="user/create" class="btn btn-primary">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Email</th>
        <th scope="col">Password</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($user as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->email}}</td>
                <td>{{$value->password}}</td>
                <td>
                    <form action="/user/{{$value->id}}" method="POST">
                    <a href="/user/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/user/{{$value->id}}" class="btn btn-primary">Edit</a>
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection